This repository contains the encX24j600 Ethernet driver for Linux (no, one didn't already exist!)  The driver was developed for the Beaglebone platform to allow integration of the j600 using the spi interface.

To build:  Use 'make menuconfig' and find the j600 in the network driver menu.

Driver has been tested and is fully working.

Bryan Wilcutt
4.4.2017